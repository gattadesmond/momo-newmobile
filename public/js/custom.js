var htmlElement = document.documentElement;
var bodyElement = document.body;
var pageOverlay = document.getElementById("overlay");

var FOODY = {
  // _API: "https://publicapi.vienthonga.vn",
  // _URL: "https://vienthonga.vn"
};

var Layout = (function () {
  var setUserAgent = function () {
    var userAgent = navigator.userAgent || navigator.vendor || window.opera;
    // Windows Phone must come first because its UA also contains "Android"
    if (/windows phone/i.test(userAgent)) {
      bodyElement.classList.add("window-phone");
      return;
    }

    if (/android/i.test(userAgent)) {
      bodyElement.classList.add("android");
      return;
    }

    // iOS detection from: http://stackoverflow.com/a/9039885/177710
    if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
      bodyElement.classList.add("ios");
      return;
    }
  };

  var viewportHeight = function () {
    // First we get the viewport height and we multiple it by 1% to get a value for a vh unit
    let vh = window.innerHeight * 0.01;
    // Then we set the value in the --vh custom property to the root of the document
    document.documentElement.style.setProperty("--vh", `${vh}px`);

    // We listen to the resize event
    window.addEventListener("resize", () => {
      // We execute the same script as before
      let vh = window.innerHeight * 0.01;
      document.documentElement.style.setProperty("--vh", `${vh}px`);
    });
  };

  var headerHideAuto = function () {
    var mainHeader = $(".topbar-auto-hide");
    var scrolling = false,
      previousTop = 0,
      currentTop = 0,
      scrollDelta = 10,
      scrollOffset = 150;
    function autoHideHeader() {
      var currentTop = $(".page-content").scrollTop();

      if (previousTop - currentTop > scrollDelta) {
        mainHeader.removeClass("is-hidden");
      } else if (
        currentTop - previousTop > scrollDelta &&
        currentTop > scrollOffset
      ) {
        mainHeader.addClass("is-hidden");
        var $payons = Array.from(document.querySelectorAll(".payon"));
        $payons.forEach(function ($el) {
          $el.classList.remove("is-active");
        });
      }

      previousTop = currentTop;
      scrolling = false;
    }

    $(".page-content").on("scroll", function () {
      if (!scrolling) {
        scrolling = true;
        !window.requestAnimationFrame
          ? setTimeout(autoHideHeader, 250)
          : requestAnimationFrame(autoHideHeader);
      }
    });
  };

  var scrollToTop = function () {
    var scrollTop = $("#scrollToTop");

    $(window).on("scroll", function () {
      if ($(window).scrollTop() > 200) {
        scrollTop.addClass("active");
      } else {
        scrollTop.removeClass("active");
      }
    });

    scrollTop.click(function () {
      $("body, html").animate(
        {
          scrollTop: 0,
        },
        500
      );
    });
  };

  return {
    init: function () {
      setUserAgent();
      scrollToTop();
      headerHideAuto();
    },
  };
})();

$(document).ready(function () {
  Layout.init();

  var homeSwiper = new Swiper(".home-swiper", {
    lazy: {
      loadPrevNext: true,
    },
    pagination: {
      el: ".swiper-pagination",
      clickable: false,
    },
  });

  var homeSwiper = new Swiper(".one-swiper", {
    lazy: true,
    pagination: {
      el: ".swiper-pagination",
      clickable: false,
    },
  });

  var htuSwiper = new Swiper(".htu-slider", {
    loop: false,
    // Disable preloading of all images
    preloadImages: false,
    // Enable lazy loading
    lazy: {
      loadPrevNext: true,
    },
    slidesPerView: 1,

    effect: "fade",
    fadeEffect: {
      crossFade: true,
    },
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
  });

  $(".htu-modal").on("shown.bs.modal", function () {
    console.log("dwdw");
    var mySwiper = this.querySelector(".swiper-container").swiper;
    console.log(mySwiper);
    mySwiper.update();

    // $(this).find(".htu-slider").swiper().update();
  });

  var cashbackHoriSwiper = new Swiper(".swiper-cashback-hori", {
    slidesPerView: "auto",
    spaceBetween: 20,
    pagination: {
      // el: '.swiper-pagination',
      clickable: true,
    },
  });

  var VtNewSwiper = new Swiper(".swiper-vt-new", {
    slidesPerView: "auto",
    spaceBetween: 10,
    pagination: {
      // el: '.swiper-pagination',
      clickable: true,
    },
  });

  // $('.dropdown-menu.keep-open').on('click', function (e) {
  //   e.stopPropagation();
  // });

  // Dropdowns

  var $payons = Array.from(document.querySelectorAll(".payon"));

  if ($payons.length > 0) {
    $payons.forEach(function ($el) {
      $el
        .querySelector(".payon-trigger")
        .addEventListener("click", function (event) {
          event.stopPropagation();

          if ($el.classList.contains("is-active")) {
            // closePayons();
            $el.classList.remove("is-active");
          } else {
            // closePayons();
            closePayons();

            $el.classList.add("is-active");
          }
        });
    });

    // document.addEventListener('click', function (event) {
    //   closePayons();
    // });
  }

  function closePayons() {
    $payons.forEach(function ($el) {
      $el.classList.remove("is-active");
    });
  }

  $(".modal").on("show.bs.modal", function (e) {
    closePayons();
  });
});

$(document).ready(function () {
  $(".js-donation-gallery").magnificPopup({
    delegate: "a",
    type: "image",
    tLoading: "Loading image #%curr%...",
    mainClass: "mfp-img-mobile",
    gallery: {
      enabled: true,
      navigateByImgClick: true,
      preload: [0, 1], // Will preload 0 - before current, and 1 after the current image
    },
    image: {
      tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
    },
  });

  var saleMoreCon = Array.from(document.querySelectorAll(".js-pron-viewmore"));
  saleMoreCon.forEach(function (e) {
    contentLoadMore(e);
  });

  $(".modal-cashback-detail").on("shown.bs.modal", function (e) {
    if ($(this).hasClass("again")) return;
    $(this).addClass("again");
    var tart = this.querySelector(".js-pron-viewmore");
    contentLoadMore(tart);
  });
});

function contentLoadMore(e) {
  const saleMore = e.querySelector(".js-pron-viewmore-content");
  const saleMoreBtn = e.querySelector(".js-pron-viewmore-btn");
  const saleMoreHeight = saleMore.scrollHeight;
  if (saleMoreHeight > 150) {
    e.classList.add("active");
    e.classList.add("is-long");

    saleMoreBtn.addEventListener("click", function () {
      if (e.classList.contains("active")) {
        saleMore.style.setProperty("max-height", saleMoreHeight + "px");
        e.classList.remove("active");
      } else {
        saleMore.style.setProperty("max-height", "150px");
        e.classList.add("active");
      }
    });
  }
}

function debounce(func, wait, immediate) {
  var timeout;
  return function () {
    var context = this,
      args = arguments;
    var later = function () {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
}

function autocompleteSearch(inp) {
  const searchQuickInput = document.querySelector(".search-quick-input");
  const searchQuickSuggestions = document.querySelector(".search-quick");

  const searchRecent = document.querySelector(".search-suggest");

  const urlMomo = "/__post/Search/Suggest";
  //const urlMomo = "http://www.mocky.io/v2/5ec7a32e2f00004b00427316";

  // const githubAjax = "https://api.github.com/search/repositories";

  const searchQuickIcon = [
    {
      name: "Keyword",
      icon: "search",
    },
    {
      name: "MomoPage",
      icon: "file-text",
    },
    {
      name: "Service",
      icon: "file-text",
    },
    {
      name: "PlaceBrand",
      icon: "file-text",
    },
    {
      name: "Promotion",
      icon: "file-text",
    },
    {
      name: "CashBackCategory",
      icon: "file-text",
    },
    {
      name: "Guide",
      icon: "help-circle",
    },
    {
      name: "QA",
      icon: "help-circle",
    },
    {
      name: "Article",
      icon: "file-text",
    },
    {
      name: "Blog",
      icon: "file-text",
    },
  ];

  var currentFocus;

  function addActive(x) {
    /*a function to classify an item as "active":*/
    if (!x) return false;
    /*start by removing the "active" class on all items:*/
    removeActive(x);
    if (currentFocus >= x.length) currentFocus = 0;
    if (currentFocus < 0) currentFocus = x.length - 1;
    /*add class "autocomplete-active":*/
    x[currentFocus].classList.add("is-active");
  }
  function removeActive(x) {
    /*a function to remove the "active" class from all autocomplete items:*/
    for (var i = 0; i < x.length; i++) {
      x[i].classList.remove("is-active");
    }
  }
  function closeAllLists(elmnt) {
    /*close all autocomplete lists in the document,
      except the one passed as an argument:*/
    var x = document.querySelectorAll(".search-quick-item");

    for (var i = 0; i < x.length; i++) {
      if (elmnt != searchQuickInput) {
        x[i].parentNode.removeChild(x[i]);
      }
    }
  }

  function displayMatches(matchArray, txt) {
    const html = matchArray
      .map((item) => {
        const regex = new RegExp(txt, "gi");
        const cont = item.Name.replace(
          regex,
          `<strong class="">${txt}</strong>`
        );
        const link = item.UrlRewrite;
        const type = item.Type;
        let icon = "";
        searchQuickIcon.forEach(function (e) {
          if (e.name == type) {
            icon = e.icon;
          }
        });

        return `
              <li role="presentation" class="search-quick-item">
                  <a href="${link}" class="search-quick-link" target="_self" role="menuitem" tabindex="-1">
  


                      <img src="/momo2020/img/icons/momo/${icon}.svg" class="icon"/>
  
                      <span data-purpose="label"> ${cont} </span>
                  </a>
              </li>
          `;
      })
      .join("");
    searchQuickSuggestions.innerHTML = html;
  }

  var searchAjaxResult = debounce(function (e) {
    //   var txt = this.value.toLowerCase();
    var txt = this.value;

    //if (!txt) {
    //  txt = "circle";
    //}

    if (txt.length < 3 && txt) {
      closeAllLists();
      return;
    }

    currentFocus = -1;
    $.ajax({
      url: urlMomo,
      headers: {
        "X-Requested-With": "XMLHttpRequest",
        "Content-Type": "application/x-www-form-urlencoded",
      },
      method: "POST",
      dataType: "json",
      cache: !1,
      data: {
        q: txt,
        c: "3", //Total shit
      },
      success: function (data) {
        // Data lấy đc về
        let dataGet = [];

        for (var property1 in data) {
          dataGet = dataGet.concat(data[property1].Items);
        }
        displayMatches(dataGet, txt);
      },
      error: function (error) {
        console.log(error);
      },
    });
  }, 300);

  document.addEventListener("click", function (e) {
    closeAllLists(e.target);
  });

  searchQuickInput.addEventListener("keydown", function (e) {
    var x = document.querySelector(".search-quick");
    if (x) x = x.getElementsByClassName("search-quick-item");

    if (e.keyCode == 40) {
      e.preventDefault();

      currentFocus++;
      addActive(x);
    } else if (e.keyCode == 38) {
      e.preventDefault();

      currentFocus--;
      addActive(x);
    } else if (e.keyCode == 13) {
      if (currentFocus > -1) {
        e.preventDefault();
        if (x) {
          location.href = x[currentFocus]
            .querySelector(".search-quick-link")
            .getAttribute("href");
        }
      }
    }
  });
  searchQuickInput.addEventListener("input", searchAjaxResult);
  searchQuickInput.addEventListener("focus", searchAjaxResult);
}
autocompleteSearch();

var menuItem = $(".nav-item.has-dropdown");
var menuTimeout;
menuItem.hover(
  function () {
    var $this = $(this);
    menuTimeout = setTimeout(function () {
      $this.addClass("is-active");
    }, 100);
  },
  function () {
    clearTimeout(menuTimeout);
    $(this).removeClass("is-active");
  }
);

var menuMobileBtn = document.querySelector("#mobile-menu__btn");
var menuMobileElement = document.querySelector(".mobile-menu__wrap");

menuMobileBtn &&
  menuMobileBtn.addEventListener("click", function (e) {
    e.preventDefault();

    if (menuMobileElement.classList.contains("is-open")) {
      menuMobileElement.classList.remove("is-open");

      menuMobileBtn.classList.remove("is-active");
      document.body.classList.remove("menu-open");
      // menuMobileElement.style.display = "none";
    } else {
      document.body.classList.add("menu-open");
      // menuMobileElement.style.display = "block";
      menuMobileElement.classList.add("is-open");
      menuMobileBtn.classList.add("is-active");
    }
  });


  //App download
// var appDownload = $(".app-download");
// $(".page-content").on("scroll", function () {
//   if ($(".page-content").scrollTop() > 300) {
//     appDownload.addClass("is-active");
//   } else {
//     appDownload.removeClass("is-active");
//   }
// });

